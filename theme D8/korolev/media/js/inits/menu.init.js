(function ($, Drupal) {
  Drupal.behaviors.menu = {
    attach: function (context) {
      $('.icon-burger').click(function () {
        $(this).stop().fadeOut(200, function () {
          $(this).next().stop().animate({
            right: 0
          }, 300);
        })
      });
      $('.menu-block .close').click(function () {
        $(this).parent().stop().animate({
          right: '-340px'
        } , 300, function () {
          $('.icon-burger').stop().fadeIn(200)
        });
      })
    }
  };

})(jQuery, Drupal);

