(function ($, Drupal) {
    Drupal.behaviors.menu = {
        attach: function (context) {

            /* LANG MENU */

            $(function () {
                $el_languages = $('.languages');
                $el_languages.hover(function () {
                    $el_languages.find('li').show();
                }, function () {
                    $el_languages.find('li:not(.is-active)').hide();
                });
            });

            /* HEADER SUBMENU */

            var item = $('.has-submenu');
            var wrapper = $('.submenu-wrapper');
            var submenu = $('.submenu');

            $(item).mouseover(
                function(){
                    $(this).find('.submenu-wrapper').stop().slideDown();
                }
            );

            $(wrapper).mouseleave(
                function(){
                    $(wrapper).stop().slideUp();
                }
            );

            /* HERO SLIDER */
            var heroSlider = new Swiper ('.hero-slider .swiper-container', {
                slidesPerView: 1,
                loop: true,
                autoplay: {
                    delay: 5000,
                },
            });

            /* SCROLL TO ELEMENT */
            $(".hero-slider-bottom-pin").click(function() {
                $('html, body').animate({
                    scrollTop: $('.hero-slider').outerHeight() + $("#header").outerHeight()
                }, 1000);
                return false;
            });

            $(".hero__bottom-pin").click(function() {
                $('html, body').animate({
                    scrollTop: $('.hero').outerHeight() + $("#header").outerHeight()
                }, 1000);
                return false;
            });

            /* TERRITORIES MAP */

            var markers = $('.territories-marker-pin');

            markers.each(function(){
                $(this).hover(
                    function(){
                        $(this).siblings('.territories-pin-popup').stop().slideDown();
                    },
                    function(){
                        $(this).siblings('.territories-pin-popup').stop().slideUp();
                    }
                );
            });

            /* NEWS SLIDER */
            var newsSlider = new Swiper ('.news-slider', {
                //slidesPerView: 3,
                //slidesPerView: 'auto',
                spaceBetween: 32,
                navigation: {
                    nextEl: '.new-control-buttons-next',
                    prevEl: '.new-control-buttons-prev',
                },
                pagination: {
                    el: '.news__slider-pagination',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    1025: {
                        slidesPerView: 'auto',
                        spaceBetween: 32,
                    },
                    768: {
                        slidesPerView: 'auto',
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                }
            });

            $('.places-blocks').each(function(p) {
                $(this).addClass('places-blocks_'+p)
                var _this = $(this);
                var p = new Swiper ($('.places-blocks_'+p).find('.swiper-container'), {
                    slidesPerView: 'auto',
                    spaceBetween: 32,
                    pagination: {
                        el: _this.find('.places-blocks__slider-pagination'),
                        type: 'bullets',
                        clickable: true
                    },
                    breakpoints: {
                        768: {
                            spaceBetween: 32,
                        },
                        320: {
                            spaceBetween: 12,
                        },
                    }
                });
            });

            /* cross goals SLIDER */
            var crossgoalsSlider = new Swiper ('.crossposts__slider .swiper-container', {
                loop: true,
                slidesPerView: 2,
                spaceBetween: 32,
                navigation: {
                    nextEl: '.crossposts__button_next',
                    prevEl: '.crossposts__button_prev',
                },
                breakpoints: {
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 32,
                    },
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    },
                }
            });

            /* team SLIDER */
            var teamSlider = new Swiper ('.team-slider .swiper-container', {
                loop: true,
                slidesPerView: 'auto',
                spaceBetween: 32,
                navigation: {
                    nextEl: '.team__button_next',
                    prevEl: '.team__button_prev',
                },
                pagination: {
                    el: '.team__slider-pagination',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    1025: {
                        spaceBetween: 32,
                    },
                    320: {
                        spaceBetween: 12,
                    },
                }
            });

            var themesSlider = new Swiper ('.themes-menu-list .swiper-container', {
                loop: false,
                slidesPerView: 4,
                spaceBetween: 32,
                centeredSlides: false,
                pagination: {
                    el: '.themes__slider-pagination',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    1025: {
                        loop: false,
                        slidesPerView: 4,
                        spaceBetween: 32,
                        centeredSlides: false,
                    },
                    640: {
                        loop: true,
                        slidesPerView: 'auto',
                        spaceBetween: 24,
                        centeredSlides: true,
                    },
                    320: {
                        loop: true,
                        slidesPerView: 'auto',
                        spaceBetween: 12,
                        centeredSlides: true,
                    },
                }
            });

            var mainCatsSlider = new Swiper ('.main-cats-slider .swiper-container', {
                loop: false,
                slidesPerView: 'auto',
                spaceBetween: 0,
                breakpoints: {
                    1025: {
                        loop: false,
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                    320: {
                        loop: true,
                        slidesPerView: 'auto',
                        spaceBetween: 20,
                        centeredSlides: true,
                    },
                },
            });

            var territoriesSlider = new Swiper ('.territories-mobile .swiper-container', {
                loop: false,
                slidesPerView: 1,
                spaceBetween: 0,
                pagination: {
                    el: '.territories-mobile__slider-pagination',
                    type: 'bullets',
                    clickable: true
                },
            });

            /* DOCUMENTATION SCROLL */
            if($(window).width() > 1024) {
                $('.documentation-result__content-wrapper').niceScroll({
                        smoothscroll: true,
                        background: "#66a8bc",
                        cursorcolor: "#126178",
                        cursorborder: "none",
                        cursorwidth: "5px"
                    });

                $('.documentation-result__content-wrapper').getNiceScroll().hide();
            }

            /* DOCUMENTATION TAB */

            var tabs = $('.documentation-result__tabs-item a');
            var content = $('.documentation-result__content-wrapper');
            var active = 'active-tab';
            var init = 'init-tab';

            function switchTab(tab) {
                var clicked = $(tab).data('tab');

                $(tabs).each(function(){
                    $(this).parent().removeClass(active + " " + init);
                });
                $(tab).parent().addClass(active);

                $(content).each(function(){
                    $(this).hide();
                });
                $('.'+clicked).slideDown();
            }
            tabs.each(function(){
                $(this).click(function(e){
                    e.preventDefault();
                    switchTab(this);
                });
            });

            $('.documentation-result__content-wrapper').getNiceScroll().show();

            $(document).on("click", ".page-info__more", function(){
                $(this).hide();
                $(this).parent().find(".page-info__text").toggleClass("truncated");
            });

            $(document).on("click", ".places-tabs__item", function(){
                if(!$(this).hasClass("active")) {
                    $(this).addClass("active").siblings().removeClass("active");

                    var num = $(this).attr('data-places');

                    $(".places-blocks__list_interactive").removeClass("active");
                    $(".places-blocks__list_interactive_"+num).addClass("active");
                }
            });

            $(document).on("click", "[data-modal]", function(){
                $('.modal-popup').addClass("active");
                return false;
            });

            $(document).on("click", ".modal-popup__close, .modal-popup__overlay", function(){
                $('.modal-popup').removeClass("active");
            });

            $(document).on("click", ".mobile-menu__close, .mobile-menu__overlay, .header__menu-trigger .menu-trigger__wrapper", function(){
                $(".mobile-menu").toggleClass("active");
                $("body").toggleClass("of-hidden");
            });

            /* MAIN HERO HEIGHT */
            $(window).on("load resize", function(){
                var hh = $('#header').outerHeight();
                var wh = $(window).height();

                $('.hero-slider__slide-wrapper').css("min-height", wh - hh + 'px');
                $('.hero__wrapper').css("min-height", wh - hh + 'px');
            });
        }
    };
})(jQuery, Drupal);