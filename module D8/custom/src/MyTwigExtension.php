<?php
namespace Drupal\custom;

/**
 * Class DefaultService.
 *
 * @package Drupal\MyTwigModule
 */
class MyTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'custom_extensions';
  }

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('display_block',
        array($this, 'display_block'),
        array('is_safe' => array('html')
        )),
      new \Twig_SimpleFunction('getImageVideo',
        array($this, 'getImageVideo'),
        array('is_safe' => array('html')
        )),
      new \Twig_SimpleFunction('getNewsList',
        array($this, 'getNewsList'),
        array('is_safe' => array('html')
        )),
    new \Twig_SimpleFunction('getNewsCategoryList',
        array($this, 'getNewsCategoryList'),
        array('is_safe' => array('html')
        )),
      new \Twig_SimpleFunction('getLinkVideo',
        array($this, 'getLinkVideo'),
        array('is_safe' => array('html')
        ))
    );
  }

  /**
   * The php function to load a given block
   */
  public function display_block($block_id) {
    $block = \Drupal\block\Entity\Block::load($block_id);
    return \Drupal::entityManager()->getViewBuilder('block')->view($block);
  }

  public function getImageVideo($item) {
    if(substr_count($item, 'youtube')>0) {
      $youtube_video_link = explode('embed/', $item);
      $video_id = $youtube_video_link[1];
      $link = 'https://img.youtube.com/vi/'.$video_id.'/maxresdefault.jpg';
    }
    else {
      $vimeo_video_link = explode('video/', $item);
      $data = file_get_contents("http://vimeo.com/api/v2/video/$vimeo_video_link[1].json");
      $data = json_decode($data);
      $link = $data[0]->thumbnail_large;
    }
    return $link;
  }

  public function getLinkVideo($item){
    if(substr_count($item, 'youtube')>0) {
      $youtube_video_link = explode('embed/', $item);
      return 'https://www.youtube.com/watch?v='.$youtube_video_link[1];
    }
    else {return $item;}
  }

  public function getNewsList($id){
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'news')
        ->condition('nid', $id, '!=')
        ->sort('created', 'DESC')
        ->range(0, 3)
      ->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);
    return $nodes;
  }
    public function getNewsCategoryList(){
        $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
        $tids = \Drupal::entityQuery('taxonomy_term')
            ->condition('vid', 'news_category')
            ->sort('weight', 'DESC')
            ->execute();
        $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
        $termList = array();

        foreach($terms as $k=>$term) {
            if($term->hasTranslation($language)){
                $tid = $term->id();
                if(isset($tid)) {
                    $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
                    $termList[$tid] = array();
                    $termList[$tid]['name'] = $translated_term->getName();
                    //$termList[$tid] = $translated_term;
                    $uri = $term->field_icon->entity->getFileUri();
                    $termList[$tid]['icon'] = file_create_url($uri); //TODO: Not translated Image
                }

            }
        }

        return $termList;
    }
}
